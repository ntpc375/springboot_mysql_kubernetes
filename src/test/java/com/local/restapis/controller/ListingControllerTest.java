package com.local.restapis.controller;

import com.local.restapis.model.Listing;
import com.local.restapis.service.ListingsService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
  public class ListingControllerTest {

    private ListingController listingController;

    @Test
    public void testGetAllListings(){
       //given
        ListingsService mockService = Mockito.mock(ListingsService.class);
        listingController = new ListingController();
        listingController.setListingsService(mockService);
       Mockito.when(mockService.getListings()).thenReturn(getListings());

       //when
        List<Listing> actualListings = listingController.getListings();

       //then
       Assert.assertEquals(actualListings.size(), getListings().size());
    }

    @Test
    public void testGetListingsByZipCode(){
        //given
        ListingsService mockService = Mockito.mock(ListingsService.class);
        listingController = new ListingController();
        listingController.setListingsService(mockService);
        Mockito.when(mockService.getAllListingsByZipCode(any(String.class))).thenReturn(getListings());

        //when
        List<Listing> actualListings = listingController.getListingsByZipCode("89822");

        //then
        Assert.assertEquals(actualListings.get(0).getZip(), getListings().get(0).getZip());
    }

    @Test
    public void testPostCSV(){
        //given
        ListingsService mockService = Mockito.mock(ListingsService.class);
        listingController = new ListingController();
        listingController.setListingsService(mockService);
        //Mockito.when(mockService.saveAllCSVData(any(String.class))).thenReturn("");
        //Mockito.doNothing().when(mockService.saveAllCSVData(any(List.class));

        //when
        String actualListings = listingController.migrateCSV();

        //then
        Assert.assertEquals("File successfully imported", actualListings);
    }


    private List<Listing> getListings() {
        List<Listing> listings = new ArrayList<>();
        listings.add(buildListing());
        return listings;
    }

    private Listing buildListing() {
        Listing listing = new Listing();
        listing.setId(1);
        listing.setAddress("Grand Canyon");
        listing.setCity("Phoenix");
        listing.setState("Arizona");
        listing.setZip("89822");
        listing.setCost("132123123");
        return listing;
   }

}

