//package com.local.restapis.service;
//
//import com.local.restapis.model.Listing;
//import com.local.restapis.repository.ListingRepository;
//import org.junit.Assert;
//import org.junit.Test;
//import org.mockito.Mockito;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ListingServiceTest {
//
//    private ListingsService listingsService;
//
//    @Test
//    public void testGetAllListings(){
//        //given
//        ListingRepository mockRepo = Mockito.mock(ListingRepository.class);
//        listingsService = new ListingsService();
//        listingsService.setListingRepository(mockRepo);
//        Mockito.when(mockRepo.findAll()).thenReturn(getListings());
//
//        //when
//        List<Listing> actualListings = listingsService.getAllListings(pageable);
//
//        //then
//        Assert.assertEquals(actualListings.size(), getListings().size());
//    }
//
//    private List<Listing> getListings() {
//        List<Listing> listings = new ArrayList<>();
//        listings.add(buildListing());
//
//        return listings;
//    }
//
//    private Listing buildListing() {
//        Listing listing = new Listing();
//        listing.setId(1);
//        listing.setAddress("Grand Canyon");
//        listing.setCity("Phoenix");
//        listing.setState("Arizona");
//        listing.setZip("89822");
//        listing.setCost("132123123");
//        return listing;
//    }
//
//}
