package com.local.restapis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="listings")
public class Listing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="address")
    private String address;

    @Column(name="city")
    private String city;

    @Column(name="state")
    private String state;

    @Column(name="cost")
    private String cost;

    @Column(name="zip")
    private String zip;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Listing listing = (Listing) o;
        return address.equals(listing.address) && city.equals(listing.city) && state.equals(listing.state) && cost.equals(listing.cost) && zip.equals(listing.zip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, city, state, cost, zip);
    }
//TODO : override equals and hashcode

}
