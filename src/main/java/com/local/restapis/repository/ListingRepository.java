package com.local.restapis.repository;

import com.local.restapis.model.Listing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingRepository extends JpaRepository<Listing, Long> {
    List<Listing> findByZip(String zipCode);

    Listing findById(long id);
}