package com.local.restapis.service;

import com.local.restapis.exception.ResourceAlreadyExists;
import com.local.restapis.exception.ResourceNotFoundException;
import com.local.restapis.model.Listing;
import com.local.restapis.repository.ListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ListingsService {

    public void setListingRepository(ListingRepository listingRepository) {
        this.listingRepository = listingRepository;
    }

    @Autowired
    ListingRepository listingRepository;

    public List<Listing> getListings() {
        return listingRepository.findAll();
    }

    public Page<Listing> getAllListings(Pageable pageable) {
        return listingRepository.findAll(pageable);
    }

    public void saveListing(Listing listing) {
        Listing listing1 = getListingById(listing.getId());
        if(listing1 != null) {
            throw new ResourceAlreadyExists("There is already listing with that Id");
        }
        listingRepository.save(listing);
    }

    public Listing getListingById(long id) {
        Listing listing = listingRepository.findById(id);
        if(listing == null) {
            throw new ResourceNotFoundException("There is no listing for the given Id");
        }

        return listing;
    }

    public List<Listing> getAllListingsByZipCode(String zip) {
        return listingRepository.findByZip(zip);
    }

    public void saveAllCSVData(Set<Listing> allCSVListings) {
        listingRepository.saveAll(allCSVListings);
    }
}