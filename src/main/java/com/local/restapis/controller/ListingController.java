package com.local.restapis.controller;

import com.local.restapis.exception.*;
import com.local.restapis.model.Listing;
import com.local.restapis.service.ListingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@RequestMapping("/v1/listings")
@RestController
@Slf4j
public class ListingController {

    @Autowired
    private ListingsService listingsService;

    @GetMapping()
    public List<Listing> getListings(){
        log.info("Inside getAllListings method of ListingController");
        List<Listing> allListings = listingsService.getListings();
        return allListings;
    }

    @GetMapping("/getWithPagination")
    public ResponseEntity<Page<Listing>> getAllListings(final Pageable pageable, @RequestAttribute("traceId") final String traceId){
        log.info("Inside getAllListings method of ListingController");
        Page<Listing> allListings = listingsService.getAllListings(pageable);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("trace-id", traceId);
        return new ResponseEntity<>(allListings, httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/id")
    public ResponseEntity<Listing> getListingById(@RequestParam(required = true, name="id") long id, @RequestAttribute("traceId") final String traceId) {
        log.info("Inside getListingById method of ListingController, trace id:{}", traceId);
        Listing listingById = listingsService.getListingById(id);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("trace-id", traceId);
        return new ResponseEntity<>(listingById, httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/addListing")
    public String saveListing(@RequestBody Listing listing) {
        log.info("Inside saveListing method of ListingController");
        //TODO: generate transaction ID
         listingsService.saveListing(listing);
         log.info("Inside saveListing method of ListingController");
         return "Listing added successfully";
    }


    @GetMapping("/getByZip")
    public List<Listing> getListingsByZipCode(@RequestParam(required = true, name = "zip") String zipCode){
        log.info("Inside getListingsByZipCode method of ListingController");
        return listingsService.getAllListingsByZipCode(zipCode);
    }

    @PostMapping("/migrateCSVData")
    public String migrateCSV() {
        String path = "classpath:listings.csv";
        String line = "";
        Set<Listing> allCSVListings = new HashSet<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile(path)));
            while((line = br.readLine()) != null) {
                String[] values = line.split(",");

                Listing listing = new Listing();
                listing.setAddress(values[0]);
                listing.setCity(values[1]);
                listing.setState(values[2]);
                listing.setCost(values[3]);
                listing.setZip(values[4]);

                allCSVListings.add(listing);
            }
        } catch (FileNotFoundException exception) {
            throw new FileNotFoundException("File not found in the specific path");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //saveListings
        listingsService.saveAllCSVData(allCSVListings);
        log.info("Inside migrateCSV method of ListingController");

        return "File successfully imported";
    }

    @RequestMapping(value = "/testExceptionHandling",
            method = RequestMethod.GET)
    public String testExceptionHandling(@RequestParam int code) {
        switch (code) {
            case 401:
                throw new UnauthorizedException("You are not authorized");
            case 404:
                throw new ResourceNotFoundException("Requested resource is not found.");
            case 400:
                throw new CustomException("Please provide resource id.");
            case 409:
                throw new ResourceAlreadyExists("Resource already exists in DB.");

            default:
                return "Yeah...No Exception.";

        }
    }

    public void setListingsService(ListingsService listingsService) {
        this.listingsService = listingsService;
    }

}
